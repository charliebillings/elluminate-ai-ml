# eClinical Solutions RBM 

## Purpose

The purpose of this project is to assess site risk for clinical trials by extracting Key Risk Indicators (KRIs) from historical clinical data. Using those KRI scores develop models to assess the real time risk of active sites in a clinical trial.

## Techniques and Methods

- Statistical analysis
- Risk based modeling
- Regression
  - LightGBM
- Outlier Detection
  - Isolation Forest
- Model Validation

## Project Description

### Inputs

In SDTM format, unless otherwise noted

* **Demographics (DM) Table:** contains patient demographic information, the site and country data are most relevant
*  **Exposure (EX) Table:** contains the Investigational Product (IP) dosage information, used to deduce the active population of sites and various IP-related KRIs
* **Adverse Event (AE) Table:** contains adverse event patient information, used to compute several KRIs
* **Lab (LB) Table:** contains the lab test information for the study, used to compute data risk scores and relevant KRIs
* **Vital Sign (VS) Table:** contains the vital sign test information for the study, used to compute data risk scores and relevant KRIs
* **Query (QY) Table:** contains the data query information for the trial, in the Rave ODM format, used to compute relevant KRIs

### Outputs

* **KRI Table:** database of KRI scores over time, stored in schema similar to SDTM format
  * **Site:** the site number for the KRI
  * **Start_Time:** the beginning time for the KRI window
  * **End_Time:** the ending time for the KRI window
  * **Type:** the type of the KRI
  * **Value:** the numerical value of the KRI

Project Contents
------------

The code for computing KRIs can be found in src/classes, the relevant files are:

* `dataset.py`: a basic dataset object with a load-from-file functionality (currently loads from a csv file, however the pyodbc connector will be implmented in next iteration) and various getter methods. 
* `time_Range.py`: a time window object which allows KRIs to be computed using study day or calendar date, contains a type-checking method and an iterator method.
* `counts.py`: the body of the KRI pipeline, contains methods for counting AEs, LBs, QYs, missed IP doses, for computing QY response times, and for computing variance in patient IP dosage. Also contains the rate functions for computing the AEs, LBs, QYs, and missed doses per active patient
* `utils.py`: a collection of helper functions for preprocessing and reshaping datasets, and tallying the number of active patients per site
* `score.py`: contains the score_KRIs method, which executes the KRI computation for a given time range


# General Licensing

To the extent possible under the law, and under our agreements, [SFL Scientific](http://sflscientific.com/) retains all copyright and related or neighboring rights to this work.
