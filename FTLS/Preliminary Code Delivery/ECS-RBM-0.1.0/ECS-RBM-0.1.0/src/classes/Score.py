import pandas as pd
from Dataset import Dataset
from Time_Range import Time_Range
from utils import preprocess_data, active_subject_count
from Counts import (
    adverse_event_rates,
    query_rates,
    lab_rates,
    missed_dose_rate,
    query_response_times,
    dosage_variance,
)

import warnings

warnings.filterwarnings("ignore")


def score_KRIs(path, start_time, end_time, time_step):
    """
    Score all KRIs in a given time window.

    Args:
        path: (str) the path to the data directory. Method assumes that the study data is saved as follows:
            path/
              |_
                AE.csv
                EX.csv
                QY.csv
                LB.csv
                DM.csv

        start_time: (int, pd.Timestamp) the start time of the window
        end_time: (int, pd.Timestamp) the end time of the window
        time_step: (int, pd.Timedelta) the step for traversing the time window

    Returns:
        a Dataset containing all the KRI scores
    """
    AE = Dataset(
        path=path + "AE.csv", params={"start_day": "AESTDY", "start_date": "AESTDTC"}
    )

    EX = Dataset(
        path=path + "EX.csv", params={"start_day": "EXSTDY", "start_date": "EXSTDTC"}
    )

    QY = Dataset(
        path=path + "QY.csv",
        params={
            "start_date": "OpenDate",
            "end_date": "CloseDate",
            "subject_column": "UniqueSubjectID",
            "study_column": "StudyOID",
        },
    )

    LB = Dataset(
        path=path + "LB.csv", params={"start_day": "LBDY", "start_date": "LBDTC"}
    )

    DM = Dataset(path=path + "DM.csv", params={"site_column": "SITEID"})

    AE = preprocess_data(AE, DM)
    EX = preprocess_data(EX, DM)
    QY = preprocess_data(QY, DM)
    LB = preprocess_data(LB, DM)

    time_range = Time_Range(start_time, end_time, time_step=time_step)

    site_list = EX.get_site_array()

    dataset_list = []
    for time in time_range:
        stepped_time = time + time_range.time_step

        AE_rates = adverse_event_rates(AE, EX, time, stepped_time, params={})
        dataset_list.extend(AE_rates)

        if time_range.type == "datetime":
            QY_rates = query_rates(
                QY, EX, time, stepped_time, params={"site_list": site_list}
            )
            dataset_list.extend(QY_rates)

            QY_response_times = query_response_times(
                QY, time, stepped_time, params={"site_list": site_list}
            )
            dataset_list.extend(QY_response_times)

        LB_rates = lab_rates(LB, EX, time, stepped_time, params={})
        dataset_list.extend(LB_rates)

        missed_IP_rate = missed_dose_rate(EX, time, stepped_time)
        dataset_list.append(missed_IP_rate)

        IP_variance = dosage_variance(EX, time, stepped_time)
        dataset_list.append(IP_variance)

        subject_count = active_subject_count(
            EX, time, stepped_time, params={"name": "Value"}
        )
        dataset_list.append(subject_count)

    dataset_list_clean = list(filter(None, dataset_list))
    dataframe_list = [dataset.dataset for dataset in dataset_list_clean]
    dataframe = pd.concat(dataframe_list).reset_index(drop=True)
    out = Dataset(
        dataset=dataframe,
        params={"KRI": True, "raw": False, "time_format": time_range.type},
    )

    return out
