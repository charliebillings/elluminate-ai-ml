Getting started
===============

This is where you describe how to get set up on a clean install, including the
commands necessary to get the raw data (using the `sync_data_from_s3` command,
for example), and then how to make the cleaned, final data sets.




Running SFL Notebook Containers
-------------------------------
First you'll need to pull the latest image to your local machine. 

* tensorflow: `sflscientific/sfl_notebooks:tf`
* dask: `sflscientific/sfl_notebooks:dask`

To pull a docker image down to your local machine: `docker pull <repo>/<image>:<tag>`

For example, to pull the dask notebook: `docker pull sflscientific/sfl_notebooks:dask`

Now, you can run the image. 

The jupyter notebook container exposes port `8888` on the docker container. When you run your docker container you need to specify the port number of the container and a port number on the host machine you are using like this `-p <host-port>:<container-port>`. In this case, the container port is `8888`. 

Locally
-------

``docker run -it -p <host-port>:8888 sfl_scientific/sfl_notebooks:tf1``


Remotely
--------

This is a multi-step process. We assume the docker image is on the remote machine. So we need to ssh into the remote. 

1. SSH into remote machine: `ssh user@remote`
2. Now run the container: `user@remote: docker run -it -p <host_port>:<container_port> <repo>/<image>:<tag> jupyter lab` 
3. Open a new terminal window on your local machine and bind the ports to the remote

For example, to run the dask notebook from the remote with ssh port forward to your laptops port 8090. 


``ssh user@remote``

``user@remote: docker run -it -p 8090:8888 sflscientific/sfl_notebooks:dask jupyter lab``

``ssh -N -L localhost:8000:localhost:8090 user@remote``

Now go to `localhost:8000` on your local laptop/machine and you'll see your notebook server. You'll need the token to log-in and that token is in the output logs of the terminal you already used. This url has the jupyter token in it so you should be good to go. It looks something like this. Put the value in the token query string into the password section of the jupyter UI login. 

``http://127.0.0.1:8888/?token=4f0ea4b12d2a9cb07a722a1ee9eae96dfe3d91755e87e647``


