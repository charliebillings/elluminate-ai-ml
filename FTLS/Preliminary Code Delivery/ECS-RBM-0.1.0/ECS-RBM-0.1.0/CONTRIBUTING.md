# Contributing to SFL

## Design Principles

1. [Rules for Developing Safety-Critical Code](https://en.wikipedia.org/wiki/The_Power_of_10:_Rules_for_Developing_Safety-Critical_Code); cf. [The Power of 10](https://web.eecs.umich.edu/~imarkov/10rules.pdf) and [JPL Institutional Coding Standard](http://lars-lab.jpl.nasa.gov/JPL_Coding_Standard_C.pdf)
2. [Programming: Principles and Practice Style Guide](http://www.stroustrup.com/Programming/PPP-style-rev3.pdf)
3. [The Zen of Python](http://legacy.python.org/dev/peps/pep-0020/)
4. Simple Scales
5. Minimal dependencies
6. Re-use existing tools and libraries when reasonable
7. Leverage the economies of scale
8. Small, loosely coupled RESTful services
9. No single points of failure
10. Start with the use case
11. ... then design from the cluster operator up
12. If you haven't argued about it, you don't have the right answer yet :)
13. If it is your first implementation, you probably aren't done yet :)

Please don't feel offended by difference of opinion. Be prepared to advocate for your change and iterate on it based on feedback. Reach out to other people — we want to help.

### Headers, Footers, Parameters, Progress Bar, etc…

Software Stack recommendation:

1. USE [Dask](http://dask.pydata.org/en/latest/): Dask is a flexible parallel computing library for analytic computing: Scales from laptops to clusters and extends the size of convenient datasets from “fits in memory” to “fits on disk”.Dask can scale to a cluster of 100s of machines. It is resilient, elastic, data local, and low latency. (Can it be used for Image Processing? Mention to 3scan...)
2. [ATOM editor](https://atom.io/)
  1. Recommended ATOM packages:
  
    ```bash
      ~/.atom/packages (34)
      ├── Sublime-Style-Column-Selection@1.7.2
      ├── atom-beautify@0.28.28
      ├── atom-material-syntax@0.4.6
      ├── atom-material-syntax-dark@0.2.5
      ├── atom-material-syntax-light@0.4.5
      ├── atom-material-ui@0.8.1
      ├── autocomplete-python@1.8.8
      ├── git-plus@5.21.0
      ├── highlight-line@0.11.1
      ├── highlight-selected@0.11.2
      ├── language-html@0.46.1
      ├── language-latex@1.0.0
      ├── language-matlab@0.2.1
      ├── language-r@0.4.1
      ├── latex@0.39.0
      ├── linter@1.11.18
      ├── linter-chktex@1.3.0
      ├── linter-flake8@1.13.4
      ├── linter-gcc@0.6.15
      ├── linter-hlint@0.5.0
      ├── linter-lintr@1.1.1
      ├── linter-lua@1.0.2
      ├── linter-markdown@3.0.2
      ├── linter-matlab@1.1.0
      ├── linter-pylint@1.2.0
      ├── linter-shellcheck@1.4.0
      ├── linter-tidy@2.2.0
      ├── markdown-preview-plus@2.4.0
      ├── merge-conflicts@1.4.4
      ├── minimap@4.24.7
      ├── minimap-highlight-selected@4.4.0
      ├── pandoc@0.2.2
      ├── pigments@0.33.1
      └── project-manager@2.7.5
    ```
3. Terminal Multiplexers:
  1. [screen](https://en.wikipedia.org/wiki/GNU_Screen)
  2. [tmux](https://en.wikipedia.org/wiki/Tmux)
  3. [byobu](https://en.wikipedia.org/wiki/Byobu_(software)) (recommended)
4. If transitioning from Matlab to Python/IPython: [Stepping from Matlab to Python](http://scottsievert.com/blog/2015/09/01/matlab-to-python/) --- some good tips'n tricks.

Example of header and footer to be used with SFL Scientific code:

```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#------------------------------------------------------------------------------#
#                                   BeGiN                                      #
# Last Modified by <danieldf> on 2016-Oct-30 at 23:00:00h EDT                  #
# Copyright 2016 (C) by Daniel Doro Ferrante, <danieldf@sflscientific.com>.    #
# CC BY-NC-ND 4.0, <https://creativecommons.org/licenses/by-nc-nd/4.0/>.       #
#                                                                              #
#   Version: SemVer 1.0.0                                                      #
#   Filename: croppingPosNeg_v100.py                                           #
#   Input: loads masks from folder/directory specified by 'MASK_DIRECTORY'     #
#   Output: cropped images saved on folders/directories specified by           #
#           'POSITIVE_DIRECTORY' and 'NEGATIVE_DIRECTORY'                      #
#   Depends: NumPy, OpenCV                                                     #
#------------------------------------------------------------------------------#
#
import os, sys
import shutil, fnmatch
import re, glob
import cv2
from time import time
#
#------------------------------------------------------------------------------#
#                                 PARAMETERS                                   #
#------------------------------------------------------------------------------#
RECROP_DIRECTORY = './recrop'
MISTAKE_DIRECTORY = './recrop/mistakes'
MASK_DIRECTORY = './masks'
# IMAGE_DIRECTORY = './pics'
IMAGE_DIRECTORY = './picsPreProcessed/03-bfilter'
POSITIVE_DIRECTORY = './positive'
NEGATIVE_DIRECTORY = './negative'
cvVersion = cv2.__version__[0]
#------------------------------------------------------------------------------#
#
#
def create_folder(path):
    if not os.path.exists(path):
        os.makedirs(path)
    #
#
# Print iterations progress
def printProgress (iteration, total, prefix = '', suffix = '', decimals = 1, barLength = 100):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        barLength   - Optional  : character length of bar (Int)
    """
    formatStr       = "{0:." + str(decimals) + "f}"
    percents        = formatStr.format(100 * (iteration / float(total)))
    filledLength    = int(round(barLength * iteration / float(total)))
    bar             = '█' * filledLength + '-' * (barLength - filledLength)
    sys.stdout.write('\r%s |%s| %s%s %s' % (prefix, bar, percents, '%', suffix)),
    if iteration == total:
        sys.stdout.write('\n')
    sys.stdout.flush()
#
# check if output destination folders exist, if not, create them
create_folder( POSITIVE_DIRECTORY )
create_folder( NEGATIVE_DIRECTORY )
create_folder( RECROP_DIRECTORY )
create_folder( MISTAKE_DIRECTORY )
print(' Creating the output folders...')
#
#
# Initialize the progress bar for the main loop
imageFileListLENGTH = len( imageFileList )
barCounter = 0
printProgress(barCounter, imageFileListLENGTH, prefix = ' Progress:', suffix = 'Complete', barLength = 50)   # Initial call to print 0% progress
#
# loop over each image and associated mask files
start_time = time()
for something in smthngList:
    #
    # Update Progress Bar
    barCounter += 1
    printProgress(barCounter, imageFileListLENGTH, prefix = ' Progress:', suffix = 'Complete', barLength = 50)
#
end_time = time()
print(' Running time: ' + str(end_time - start_time) )
print(' Done masking.')
#
#
#------------------------------------------------------------------------------#
#                                    eNd                                       #
#------------------------------------------------------------------------------#
```


## Recommended workflow

1. Make your changes. Docs and tests for your patch must land before or with your patch.
2. Run unit tests, functional tests, probe tests
3. `git review`

That aside, in order to work with SFL GitHub Organization repository, the method of choice is described below,

* [Basic GitHub Workflow for Collaboration](http://blog.swilliams.me/words/2015/06/30/basic-github-workflow-for-collaboration/).

Here's the summary:

1. Everyone Forks from the Organization: If a repository is private on the Organization, your forks will be private too, and won’t count against your own private project count.
2. Clone Your Fork: Get your local copy.
3. Set up Remotes: Add a remote for the Organizations repository.
4. Work in Branches: Working on a feature? Create a branch. Fixing a bug? Create a branch. Keep `master` clean.
5. Push Branches to Origin: Done with a feature or an issue? Push it back up to `origin` (your fork).
6. Create a Pull Request: Multiple small Pull Requests are much easier to review.
7. Someone Else Reviews the Pull Request: The Pull Request review process  is used to annotate code and discuss. Merge it in if everything is good.

And here are the "rules of thumb":

1. Keep the master branch clean. That should be ready to go live if necessary. This means tests should be passing, everything compiles, nothing important should be broken, etc.
2. Never commit directly to `upstream`. Upstream should only be updated through Pull Requests. Exception: pushing tags.
3. Pull from `upstream` regularly. The more codebases diverge, the more likely a nasty merge problem will occur.
4. Keep branches small. Just reiterating it again.
5. There are exceptions to every rule. Use them intelligently.


## Code Organization

* `Code/`:
* `Content/`:
* `Legal/`:
* `Projects/`:


## Ideas

If you're working on something, it's a very good idea to write down what you're thinking about. This lets others get up to speed, helps you collaborate, and serves as a great record for future reference. Write down your thoughts somewhere and put a link to it here. It doesn't matter what form your thoughts are in; use whatever is best for you. Your document should include why your idea is needed and your thoughts on particular design choices and tradeoffs. Please include some contact information so that people can collaborate with you.
