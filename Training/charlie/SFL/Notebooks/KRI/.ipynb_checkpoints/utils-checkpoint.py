from copy import deepcopy
import pandas as pd
import numpy as np

from Dataset import Dataset
from Time_Range import Time_Range


def series_to_frame(series, Time_Range, name, params={}):
    if "value_name" in params:
        value_name = params["value_name"]
        temp = series.rename(value_name).to_frame()
    else:
        value_name = "Value"
        temp = series.rename(value_name).to_frame()

    temp = temp.reset_index()
    temp["Start_Time"] = Time_Range.start_time
    temp["End_Time"] = Time_Range.end_time
    temp["Type"] = name

    if "category" in params:
        temp["Category"] = params["category"]
        return temp[["Site", "Category", "Type", "Start_Time", "End_Time", value_name]]

    return temp[["Site", "Type", "Start_Time", "End_Time", value_name]]


def preprocess_data(raw_dataset, dm_dataset):
    if isinstance(raw_dataset, Dataset) and isinstance(dm_dataset, Dataset):
        # Clone the input dataset
        out = deepcopy(raw_dataset)
        out.raw = False

        # Convert the start and end dates if necessary
        if hasattr(raw_dataset, "start_date"):
            out.dataset[out.start_date] = pd.to_datetime(out.dataset[out.start_date])
        if hasattr(raw_dataset, "end_date"):
            out.dataset[out.end_date] = pd.to_datetime(out.dataset[out.end_date])

        # Rename the subject column if necessary
        if hasattr(out, "subject_column"):
            out.dataset = out.dataset.rename(columns={out.subject_column: "USUBJID"})

        out.subject_column = "USUBJID"

        # Rename the study column if necessary
        if hasattr(out, "study_column"):
            out.dataset = out.dataset.rename(columns={out.study_column: "STUDYID"})

        out.subject_column = "STUDYID"

        # Rename the site column if it exists in the dataset, create it otherwise
        if hasattr(out, "site_column"):
            out.dataset = out.dataset.rename(columns={out.site_column: "Site"})
        else:
            site_info = dm_dataset.dataset[["USUBJID", "SITEID", "COUNTRY"]]
            site_info = site_info.rename(columns={"SITEID": "Site"})
            out.dataset = out.dataset.merge(site_info, on="USUBJID")

        out.site_column = "Site"

        return out


def count_divide(df_row):
    count = df_row["Value"]
    num_subjects = df_row["Active_Subject_Count"]

    if num_subjects == 0:
        return np.nan
    else:
        return count / num_subjects


def count_per_active_subject(count_dataset, EX_dataset):
    if hasattr(count_dataset, "count") and count_dataset.count:
        if count_dataset.dataset.empty:
            return Dataset(dataset = count_dataset.dataset, params={"KRI": True, "raw": False})
        else:
            start_time = count_dataset.dataset["Start_Time"].iloc[0]
            end_time = count_dataset.dataset["End_Time"].iloc[0]

            if isinstance(start_time, int):
                time_format = "int"
            else:
                time_format = "datetime"

            count_name = count_dataset.dataset["Type"].iloc[0]
            base_name = "_".join(count_name.split("_")[:-1])
            name = base_name + "_per_active_subject"

            active_subject_df = active_subject_count(
                EX_dataset, start_time, end_time
            ).dataset
            count_df = count_dataset.dataset[["Site", "Value"]]
            # count_df = count_df.reindex(EX_dataset.get_site_array(), fill_value=0)
            merged_table = count_df.merge(active_subject_df, on="Site", how="outer")
            merged_table = merged_table.fillna(0)
            merged_table["Value"] = merged_table.apply(count_divide, axis=1)
            merged_table["Type"] = name
            merged_table = merged_table.drop(columns=["Active_Subject_Count"])
            merged_table = merged_table[~merged_table.Value.isna()].reset_index(
                drop=True
            )
            merged_table = merged_table[
                ["Site", "Type", "Start_Time", "End_Time", "Value"]
            ]
            out = Dataset(
                dataset=merged_table,
                params={"KRI": True, "raw": False, "time_format": time_format},
            )
            return out

    else:
        print("Data must be count type")
        raise ValueError


def active_subject_count(EX_dataset, start_time, end_time, params={}):
    if EX_dataset.raw:
        print("Data must be preprocessed before counts")
        raise ValueError

    time_window = Time_Range(start_time, end_time)
    time_column_name = EX_dataset.get_time_column_name(time_window)

    left_bool = time_window.start_time <= EX_dataset.dataset[time_column_name]
    right_bool = EX_dataset.dataset[time_column_name] < time_window.end_time

    in_range = EX_dataset.dataset[left_bool & right_bool]
    site_counts = in_range.groupby("Site")["USUBJID"].nunique()
    site_list = EX_dataset.get_site_array()
    site_counts = site_counts.reindex(site_list, fill_value=0)

    site_count_frame = series_to_frame(
        site_counts,
        time_window,
        "active_subject_count",
        params={"value_name": "Active_Subject_Count"},
    )

    out = Dataset(
        dataset=site_count_frame,
        params={"count": True, "raw": False, "time_format": time_window.type},
    )
    return out
