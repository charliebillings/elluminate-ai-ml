import pandas as pd
import numpy as np
from pandas import to_datetime as to_datetime
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import pyodbc

class PatientClassifier:
    """
    This class helps run standard classification techniques I have learned.
    """
    def __init__(self, schema=''):
        """
        Constructor
        """
        pd.set_option('mode.chained_assignment', None)
        self._schema = schema
        self._schemamapped = schema + '_ODM_Mapped#'

        self.DM = pd.DataFrame()
        self.allsubjectsdf = pd.DataFrame()
        self.subjFeaturesdf = pd.DataFrame()
        self.patientlabsscoringdf = pd.DataFrame()
        self.patientscore1df = pd.DataFrame()
        self.plotdf = pd.DataFrame()
        
        if schema:
            self.load_tables()
        else:
            raise NotImplementedError
            
    def load_tables(self):
        """
        load DM & patient scoring tables
        """
        conn = pyodbc.connect('DRIVER={SQL Server};Server=10.10.10.203;Database=sfls1_data;user=CDR_IIS_Account;password=eclinical.1;Trusted_Connection=false')

        sql = f'select * from [{self._schema}].[DM]'
        self.DM = pd.read_sql(sql, conn)
        
        self.allsubjectsdf = self.DM[~self.DM.ARMCD.eq('SCRFAIL')]
        self.subjFeaturesdf = self.allsubjectsdf.drop(columns=['STUDYID','DOMAIN','SUBJID','ETHNIC','ACTARMCD','ACTARM', 'AGEU'])
        
        sql = f"""
select 
    lb1.USUBJID, 
    lb1.LBSEQ, 
    lb1.LBTEST, 
    lb1.LBORRES, 
    lb1.LBNRIND, 
    convert(datetime, replace(lb1.LBDTC,'T',' ')) LABDATE, lb1.LBBLFL,  
    ROW_NUMBER() over(partition by lb1.USUBJID,lb1.LBTEST order by convert(datetime, replace(lb1.LBDTC,'T',' '))) [rownum],
    FIRST_VALUE(lb1.LBNRIND) over(partition by lb1.USUBJID,lb1.LBTEST order by convert(datetime, replace(lb1.LBDTC,'T',' ')) ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) [firstcondition],
    LAST_VALUE(lb1.LBNRIND) over(partition by lb1.USUBJID,lb1.LBTEST order by convert(datetime, replace(lb1.LBDTC,'T',' ')) ROWS BETWEEN CURRENT ROW AND UNBOUNDED FOLLOWING) [lastcondition],
    case 
        when LAST_VALUE(lb1.LBNRIND) over(partition by lb1.USUBJID,lb1.LBTEST order by convert(datetime, replace(lb1.LBDTC,'T',' ')) ROWS BETWEEN CURRENT ROW AND UNBOUNDED FOLLOWING) = 
        'NORMAL' then 
            case 
                when FIRST_VALUE(lb1.LBNRIND) over(partition by lb1.USUBJID,lb1.LBTEST order by convert(datetime, replace(lb1.LBDTC,'T',' ')) ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) = 'NORMAL' then 0
            else
                1
            end
        else 
            case
                when LAST_VALUE(lb1.LBNRIND) over(partition by lb1.USUBJID,lb1.LBTEST order by convert(datetime, replace(lb1.LBDTC,'T',' ')) ROWS BETWEEN CURRENT ROW AND UNBOUNDED FOLLOWING) = 
                FIRST_VALUE(lb1.LBNRIND) over(partition by lb1.USUBJID,lb1.LBTEST order by convert(datetime, replace(lb1.LBDTC,'T',' ')) ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)  then 0
            else
                -1
            end
    end as score

FROM [sfls1_data].[{self._schema}].[LB] lb1 where lb1.LBSTAT is null and not lb1.[LBNRIND] is null 
and convert(datetime, replace(lb1.LBDTC,'T',' ')) >= 
(select top 1 convert(datetime, replace(lb2.LBDTC,'T',' ')) from [sfls1_data].[{self._schema}].[LB] lb2 where lb2.USUBJID = lb1.USUBJID and lb2.LBTEST = lb1.LBTEST and lb2.LBBLFL = 'Y')
"""
        self.patientlabsscoringdf = pd.read_sql(sql, conn)
        self.patientscore1df = self.patientlabsscoringdf[self.patientlabsscoringdf.LBBLFL.eq('Y')]

    def process_ARMS(self):
        """
        In the study I have been working with there are only two arms. Not sure what to do if there are more
        """
#         print('subjFeaturesdf')
#         print(self.subjFeaturesdf.head(5))
#         print('patientlabsscoringdf')
#         print(self.patientlabsscoringdf.head(5))
#         print('patientscore1df')
#         print(self.patientscore1df.head(5))
        self.plotdf =  pd.merge(self.subjFeaturesdf, self.patientscore1df, on='USUBJID')
        print(self.plotdf.ARM.value_counts())
#             self.plotdf.replace(to_replace=['COHORT 1', 'COHORT 2'], value=[0,1])
        

