import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def sfl_defaults():
    plt.style.use("classic")
    plt.rcParams["figure.figsize"] = [8.0, 5.0]
    plt.rcParams["figure.facecolor"] = "w"

    # text size
    plt.rcParams["xtick.labelsize"] = 14
    plt.rcParams["ytick.labelsize"] = 14
    plt.rcParams["axes.labelsize"] = 15
    plt.rcParams["axes.titlesize"] = 16
    plt.rcParams["legend.fontsize"] = 12

    # grids
    plt.rcParams["grid.color"] = "k"
    plt.rcParams["grid.linestyle"] = ":"
    plt.rcParams["grid.linewidth"] = 0.5


def screen_failure_ratio_map(group_by):
    """
    Computes the ratio of screen failures and total patients screened.

    Args:
        group_by: (pandas GroupBy object) the Subject table grouped by site number

    Returns
        a pandas Series object of the screen failure ratio
    """
    value_count = group_by.value_counts()
    if "Screen Failed" in value_count:
        return value_count["Screen Failed"] / value_count.sum()
    return 0


def series_to_frame(series, kri_name, start_time, end_time, units=np.nan, category=""):
    temp = series.rename("Value").to_frame()
    temp = temp.reset_index()
    temp["Start_Time"] = start_time
    temp["End_Time"] = end_time
    temp["KRI_Type"] = kri_name
    temp["Units"] = units

    if category:
        temp["Category"] = category
        return temp[
            ["Site", "Category", "KRI_Type", "Start_Time", "End_Time", "Value", "Units"]
        ]

    return temp[["Site", "KRI_Type", "Start_Time", "End_Time", "Value", "Units"]]


def clean_units(
    df, name_column, value_column, unit_column, category_column="", thresh=0.9
):
    if not category_column:
        category_column = name_column

    df.loc[df[unit_column] == "U/L", unit_column] = (
        df.loc[df[unit_column] == "U/L", value_column] * 60
    )
    df.loc[df[unit_column] == "U/L", unit_column] = "ukat/L"

    unit_groupby = df.groupby([category_column, name_column])
    unit_count = unit_groupby[unit_column].value_counts()
    unit_exceptions_bool = unit_groupby[value_column].nunique() > 1
    unit_exceptions_count = unit_count[unit_exceptions_bool]

    unit_exception_conc_bool = (
        unit_exceptions_count.groupby([category_column, name_column]).agg(
            lambda s: s.max() / s.sum()
        )
        > thresh
    )
    dropped_df = pd.DataFrame()

    for (category, name), key_bool in unit_exception_conc_bool.items():
        if key_bool:
            dominant_unit = unit_count[category, name].idxmax()
            category_bool = df[category_column] == category
            name_bool = df[name_column] == name
            unit_bool = df[unit_column] != dominant_unit
            drop_bool = category_bool & name_bool & unit_bool

            dropped_df = dropped_df.append(df[drop_bool])
            df = df[~drop_bool]

    return df, dropped_df

