"""
Module for computing KRIs in python
"""
import warnings
import pandas as pd
import numpy as np
from pandas import to_datetime as to_datetime
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import pyodbc

from util import (
    screen_failure_ratio_map,
    series_to_frame,
    clean_units,
    sfl_defaults,
)

warnings.filterwarnings("ignore")


class Dataset:
    """
    A dataset object which contains relevant tables for KRI computation.

    Args:
        path: (str) the path to the data directory relative to the current working directory
        score: (bool) if True the loaded data is automatically scored

    Class vars:
        _dataset_path: (str) the input path to the data directory
        _site_list: (list) a list of all sites in the trial
        _last_patiemnt_dose: (pandas DataFrame) a dataframe multi-indexed by site and
            patient identifier containing the last dose date
        _site_active: (pandas DataFrame) a dataframe indexed by site containting the
            first and last dosage dates
        AE: (pandas DataFrame) a dataframe containing the Adverse Event data
        EX: (pandas DataFrame) a dataframe containing the Exposure (dosage) data
        LE: (pandas DataFrame) a dataframe containing the Lab data
        QY: (pandas DataFrame) a dataframe containing the Query (data audit) data
        SB: (pandas DataFrame) a dataframe containing the Subject data

        Safety: (pandas DataFrame) a dataframe containing all of the Safety KRIs
        Data Quality: (pandas DataFrame) a dataframe containing all of the Data Quality KRIs
        IP_Management: (pandas DataFrame) a dataframe containing all of the IP Management KRIs
        SB_Management: (pandas DataFrame) a dataframe containing all of the SB Management KRIs
        LB_Management: (pandas DataFrame) a dataframe containing all of the LB Management KRIs
        VS_Trend: (pandas DataFrame) a dataframe containing all of the VS Trend KRIs
        LB_Trend: (pandas DataFrame) a dataframe containing all of the LB Trend KRIs


    """

    def __init__(self, schema=''):
        """
        Initializes an empty Dataset object

        Args:
            path: (str) the path to the data directory relative to the current working directory
            score: (bool) if True the loaded data is automatically scored

        Returns:
            None
        """
        self._schema = schema
        self._schemamapped = schema + '_ODM_Mapped#'
        self._site_list = []
        self._last_patient_dose = pd.DataFrame()
        self._site_active = pd.DataFrame()
        self._raw_dfs = ['AE', 'EX', "LB", "QY", "SB", "DM", "VS"]
        self._KRI_dfs = ['Safety', "Data_Quality", "IP_Management", "SB_Management", "LB_Management"]

        self.AE = pd.DataFrame()
        self.EX = pd.DataFrame()
        self.LB = pd.DataFrame()
        self.QY = pd.DataFrame()
        self.SB = pd.DataFrame()
        self.DM = pd.DataFrame()
        self.VS = pd.DataFrame()

        self.Safety = pd.DataFrame()
        self.Data_Quality = pd.DataFrame()
        self.IP_Management = pd.DataFrame()
        self.SB_Management = pd.DataFrame()
        self.LB_Management = pd.DataFrame()
        self.VS_Trend = pd.DataFrame()
        self.LB_Trend = pd.DataFrame()

        if schema:
            self.load_tables()
            self.clean_dates()
            self.process_sites()

    def load_tables(self):
        """
        Reads in the clinical data from the specified data directory

        Args:
            None

        Returns:
            None

        adapt to use pyodbc
        """
        conn = pyodbc.connect('DRIVER={SQL Server};Server=10.10.10.203;Database=sfls1_data;user=CDR_IIS_Account;password=eclinical.1;Trusted_Connection=false')
        sql = f'select * from [{self._schema}].[AE]'
        self.AE = pd.read_sql(sql, conn)
        sql = f'select * from [{self._schema}].[EX]'
        self.EX = pd.read_sql(sql, conn)
        sql = f'select * from [{self._schema}].[LB]'
        self.LB = pd.read_sql(sql, conn)
#         sql = f'select * from [{self._schema}].[QY]'
        sql = f'select * from [{self._schemamapped}].[QUERY]'
        self.QY = pd.read_sql(sql, conn)
#         sql = f'select * from [{self._schema}].[SB]'
        sql = f'select * from [{self._schemamapped}].[SUBJECT]'
        self.SB =pd.read_sql(sql, conn)
        sql = f'select * from [{self._schema}].[DM]'
        self.DM = pd.read_sql(sql, conn)
        sql = f'select * from [{self._schema}].[VS]'
        self.VS = pd.read_sql(sql, conn)
        print('Data Loaded')


    def process_sites(self):
        """
        Add site and country information to each data table, renames the site info (for the SB and QY tables)
            and populates the _site_list class variable.

        Args:
            None

        Returns:
            None
        """
        site_info = self.DM[["USUBJID", "SITEID", "COUNTRY"]]
        site_info = site_info.rename(columns={"SITEID": "Site"})
        for domain in ["AE", "EX", "LB", "VS"]:
            df = getattr(self, domain)
            df = df.merge(site_info, on="USUBJID")
            setattr(self, domain, df)

        self.QY = self.QY.rename(columns={"LocationOID": "Site"})
        self.SB = self.SB.rename(columns={"LocationID": "Site"})
        self._site_list = self.EX.Site.unique()

        dose_dates = self.EX.groupby("Site").EXSTDTC
        self._site_active["StartDate"] = dose_dates.agg(min)
        self._site_active["EndDate"] = dose_dates.agg(max)

        dose_days = self.EX.groupby("Site").EXSTDY
        self._site_active["StartDay"] = dose_days.agg(min)
        self._site_active["EndDay"] = dose_days.agg(max)

    def clean_dates(self):
        """
        Recasts date strings as datetime objects, add the response time to the query table.

        Args:
            None

        Returns:
            None
        """
        self.EX.EXSTDTC = to_datetime(self.EX.EXSTDTC)
        self.AE.AESTDTC = to_datetime(self.AE.AESTDTC)
        self.LB.LBDTC = to_datetime(self.LB.LBDTC)
        self.VS.VSDTC = to_datetime(self.VS.VSDTC)

        self.QY.OpenDate = to_datetime(self.QY.OpenDate)
        self.QY.CloseDate = to_datetime(self.QY.CloseDate)

        self.QY["ResponseTime"] = self.QY.CloseDate - self.QY.OpenDate
        self.QY["ResponseTime"] = self.QY.ResponseTime.apply(
            lambda x: float(x.seconds / 3600)
        )

    def active_subject_count(self, start_time, end_time, level="table"):
        """
        Computes the number of active patients (those still recieving doses of the study drug)

        Args:
            date: (int, datetime, int) the date for the active patient count
            level: (int, int) a flag for controlling the output format.

        Returns:
            if level == 'table': (pandas Series) the active populations at all sites
            if level == 'study': (int) the total number of active subjects across all sites
            if level in _site_list: (int) the number of active patients at site level
        """
        if isinstance(start_time, int) and isinstance(end_time, int):
            in_range = self.EX[
                (start_time <= self.EX.EXSTDY) & (self.EX.EXSTDY < end_time)
            ]
        elif isinstance(start_time, pd.Timestamp) and isinstance(
            end_time, pd.Timestamp
        ):
            in_range = self.EX[
                (start_time <= self.EX.EXSTDTC) & (self.EX.EXSTDTC < end_time)
            ]
        else:
            raise NotImplementedError

        site_counts = in_range.groupby("Site").USUBJID.nunique()
        site_counts = site_counts.reindex(self._site_list, fill_value=0)
        site_counts = site_counts.rename("Subjects")

        if level == "table":
            return site_counts
        if level == "study":
            return site_counts.sum()
        if level in site_counts.index:
            return site_counts.loc[level]

    def screen_failure_ratio(self):
        """
        Computes the ratio of screen failures and total patients screened.

        Args:
            None
        Returns:
            a pandas Series containing site screen failure rates
        """
        site_failure = self.SB.groupby("Site").SubjectStatus.apply(
            screen_failure_ratio_map
        )

        out = site_failure.reindex(self._site_list, fill_value=1)

        out = series_to_frame(
            site_failure, "Screen_Falue_Ratio", np.nan, np.nan, units=np.nan
        )
        return out

    def subject_count_table(self, start_time, end_time, time_step):
        """
        Computes how active site populations decline over time.

        Args:
            start_date: (int, datetime) the inclusive start date of the time window, if
                start_date == '' the start date is set as the first date in the relevant table
            end_date: (int, datetime) the non-inclusive end date of the time window, if
                end_date == '' the end date is set to the last date in the relevant table

        Returns:
            a pandas DataFrame with columns:
                Site: the site number
                KRI_Type: the KRI information
                Start_Time: the start_time for the KRI
                End_Time: the end_time for the KRI
                Value: the value of the KRI
                Units: the units of the KRI
        """

        # start_count = self.active_subject_count(start_time, end_time)
        # end_count = self.active_subject_count(end_time)

        # sub_diff = start_count - end_count

        # out = series_to_frame(
        #     sub_diff, "Subject_Dropout", start_time, end_time, units="sub"
        # )
        # return out
        pass

    def ae_count(self, start_time, end_time, ae_type=""):
        """
        Tally the total number of Averse Events (AE) in the specified time window.

        Args:
            start_time: (datetime, int) the inclusive start date of the time window, if
                start_time == '' the start date is set as the first date in the relevant table
            end_time: (datetime, int) the non-inclusive end date of the time window, if
                end_time == '' the end date is set to the last date in the relevant table
            ae_type: (int) the type of AE to tally, options are:
                    '': all AEs
                    's': serious AEs
                    'r': relevant AEs (AEs related to the study condition)
                    'sr': serous, relevant AEs

        Returns:
            a pandas DataFrame with columns:
                Site: the site number
                KRI_Type: the KRI information
                Start_Time: the start_time for the KRI
                End_Time: the end_time for the KRI
                Value: the value of the KRI
                Units: the units of the KRI
        """
        if isinstance(start_time, int) and isinstance(end_time, int):
            in_range = self.AE[
                (start_time <= self.AE.AESTDY) & (self.AE.AESTDY < end_time)
            ]
        elif isinstance(start_time, pd.Timestamp) and isinstance(
            end_time, pd.Timestamp
        ):
            in_range = self.AE[
                (start_time <= self.AE.AESTDTC) & (self.AE.AESTDTC < end_time)
            ]
        else:
            raise NotImplementedError

        if ae_type == "s":
            ser_bool = in_range.AESER == "Y"
            in_type = in_range[ser_bool]
        elif ae_type == "r":
            rel_bool = in_range.AEREL == "RELATED"
            in_type = in_range[rel_bool]
        elif ae_type == "sr":
            ser_bool = in_range.AESER == "Y"
            rel_bool = in_range.AEREL == "RELATED"
            both_bool = ser_bool & rel_bool
            in_type = in_range[both_bool]
        else:
            in_type = in_range

        site_aes = in_type.groupby("Site").STUDYID.agg("count")
        site_aes = site_aes.reindex(self._site_list, fill_value=0)
        out = series_to_frame(
            site_aes, ae_type.upper() + "AE_count", start_time, end_time, units=np.nan
        )
        return out

    def ae_rate(self, start_time, end_time, ae_type=""):
        """
        Compute the total number of AE per active subject in the specified time window.

        Args:
            start_time: (datetime, int) the inclusive start date of the time window, if
                start_time == '' the start date is set as the first date in the relevant table
            end_time: (datetime, int) the non-inclusive end date of the time window, if
                end_time == '' the end date is set to the last date in the relevant table
            ae_type: (int) the type of AE to tally, options are:
                    '': all AEs
                    's': serious AEs
                    'r': relevant AEs (AEs related to the study condition)
                    'sr': serous, relevant AEs

        Returns:
            a pandas DataFrame with columns:
                Site: the site number
                KRI_Type: the KRI information
                Start_Time: the start_time for the KRI
                End_Time: the end_time for the KRI
                Value: the value of the KRI
                Units: the units of the KRI
        """
        active_subjects = self.active_subject_count(start_time, end_time)

        step_ae = self.ae_count(start_time, end_time, ae_type)
        step_ae = step_ae.merge(active_subjects, on="Site")
        step_ae.Value = step_ae.Value / step_ae.Subjects
        step_ae = step_ae.replace({np.inf: np.nan})
        step_ae["KRI_Type"] = ae_type.upper() + "AE_per_active_subject"
        step_ae = step_ae[step_ae.Value.notna()]
        step_ae = step_ae.drop("Subjects", axis=1)

        return step_ae

    def qy_count(self, start_time, end_time, qy_type=""):
        """
        Tally the total number of Queries (QY) in the specified time window.

        Args:
            start_time: (int, datetime) the inclusive start date of the time window, if
                start_time == '' the start date is set as the first date in the relevant table
            end_time: (int, datetime) the non-inclusive end date of the time window, if
                end_time == '' the end date is set to the last date in the relevant table
            qe_type: (int) the type of QY to tally, options are:
                    '': all QYs
                    'm': manually generated QYs (generated by data managers)
                    'a': automatically generated QYs (generated by elluminate system)

        Returns:
            a pandas DataFrame with columns:
                Site: the site number
                KRI_Type: the KRI information
                Start_Time: the start_time for the KRI
                End_Time: the end_time for the KRI
                Value: the value of the KRI
                Units: the units of the KRI
        """
        if isinstance(start_time, pd.Timestamp) and isinstance(end_time, pd.Timestamp):
            if qy_type == "h":
                in_range = self.QY[
                    (end_time <= self.QY.OpenDate) & (self.QY.CloseDate > end_time)
                ]
            else:
                in_range = self.QY[
                    (start_time <= self.QY.OpenDate) & (self.QY.OpenDate < end_time)
                ]
        else:
            raise NotImplementedError

        if qy_type == "m":
            in_type_bool = in_range.QueryRecipient != "System to Site"
            in_type = in_range[in_type_bool]
        elif qy_type == "a":
            in_type_bool = in_range.QueryRecipient == "System to Site"
            in_type = in_range[in_type_bool]
        else:
            in_type = in_range

        site_qys = in_type.groupby("Site").StudyOID.agg("count")
        site_qys = site_qys.reindex(self._site_list, fill_value=0)

        out = series_to_frame(
            site_qys, qy_type.upper() + "QY_count", start_time, end_time, units=np.nan
        )
        return out

    def qy_response_time(self, start_time, end_time, qy_type=""):
        """
        Compute the average query response rate.

        Args:
            start_date: (int, datetime) the inclusive start date of the time window, if
                start_date == '' the start date is set as the first date in the relevant table
            end_date: (int, datetime) the non-inclusive end date of the time window, if
                end_date == '' the end date is set to the last date in the relevant table

        Returns:
            a pandas DataFrame with columns:
                Site: the site number
                KRI_Type: the KRI information
                Start_Time: the start_time for the KRI
                End_Time: the end_time for the KRI
                Value: the value of the KRI
                Units: the units of the KRI
        """
        if isinstance(start_time, pd.Timestamp) and isinstance(end_time, pd.Timestamp):
            in_range = self.QY[
                (start_time <= self.QY.OpenDate) & (self.QY.CloseDate < end_time)
            ]
        else:
            raise NotImplementedError

        if qy_type == "m":
            in_type_bool = in_range.QueryRecipient != "System to Site"
            in_type = in_range[in_type_bool]
        elif qy_type == "a":
            in_type_bool = in_range.QueryRecipient == "System to Site"
            in_type = in_range[in_type_bool]
        else:
            in_type = in_range

        site_qy_response = in_type.groupby("Site").ResponseTime.mean()
        site_qy_response = site_qy_response.reindex(self._site_list, fill_value=0)

        out = series_to_frame(
            site_qy_response,
            qy_type.upper() + "QY_Response",
            start_time,
            end_time,
            units="hours",
        )

        return out

    def qy_rate(self, start_time, end_time, qy_type=""):
        """Compute the total number of Queries (QY) per active subject in the specified time window.

        Args:
            start_time: (int, datetime) the inclusive start date of the time window, if
                start_time == '' the start date is set as the first date in the relevant table
            end_time: (int, datetime) the non-inclusive end date of the time window, if
                end_time == '' the end date is set to the last date in the relevant table
            qe_type: (int) the type of QY to tally, options are:
                    '': all QYs
                    'm': manually generated QYs (generated by data managers)
                    'a': automatically generated QYs (generated by elluminate system)

        Returns:
            a pandas DataFrame with columns:
                Site: the site number
                KRI_Type: the KRI information
                Start_Time: the start_time for the KRI
                End_Time: the end_time for the KRI
                Value: the value of the KRI
                Units: the units of the KRI
        """
        active_subjects = self.active_subject_count(start_time, end_time)

        step_qy = self.qy_count(start_time, end_time, qy_type)
        step_qy = step_qy.merge(active_subjects, on="Site")
        step_qy.Value = step_qy.Value / step_qy.Subjects
        step_qy = step_qy.replace({np.inf: np.nan})
        step_qy["KRI_Type"] = qy_type.upper() + "QY_per_active_subject"
        step_qy = step_qy[step_qy.Value.notna()]
        step_qy = step_qy.drop("Subjects", axis=1)

        return step_qy

    def lb_count(self, start_time, end_time, lb_type=""):
        """
        Tally the total number of Labs (LB) in the specified time window.

        Args:
            start_time: (int, datetime) the inclusive start date of the time window, if
                start_time == '' the start date is set as the first date in the relevant table
            end_time: (int, datetime) the non-inclusive end date of the time window, if
                end_time == '' the end date is set to the last date in the relevant table
            lb_type: (int) the type of LB to tally, options are:
                    '': all LBs
                    'm': missed LBs (scheduled labs which are not done)
                    'u': uncheduled LBs

        Returns:
            a pandas DataFrame with columns:
                Site: the site number
                KRI_Type: the KRI information
                Start_Time: the start_time for the KRI
                End_Time: the end_time for the KRI
                Value: the value of the KRI
                Units: the units of the KRI
        """
        if isinstance(start_time, int) and isinstance(end_time, int):
            in_range = self.LB[(start_time <= self.LB.LBDY) & (self.LB.LBDY < end_time)]
        elif isinstance(start_time, pd.Timestamp) and isinstance(
            end_time, pd.Timestamp
        ):
            in_range = self.LB[
                (start_time <= self.LB.LBDTC) & (self.LB.LBDTC < end_time)
            ]
        else:
            raise NotImplementedError

        if lb_type == "u":
            unsched_bool = in_range.VISITNUM.apply(lambda x: x % 1) != 0
            in_type = in_range[unsched_bool]
        elif lb_type == "m":
            miss_bool = in_range.LBSTAT == "NOT DONE"
            in_type = in_range[miss_bool]
        else:
            in_type = in_range

        site_lbs = in_type.groupby("Site").STUDYID.agg("count")
        site_lbs = site_lbs.reindex(self._site_list, fill_value=0)

        out = series_to_frame(
            site_lbs, lb_type.upper() + "LB_count", start_time, end_time, units=np.nan
        )
        return out

    def lb_rate(self, start_time, end_time, lb_type=""):
        """
        Compute the total number of Labs (LB) per active subject in the specified time window.

        Args:
            start_time: (int, datetime) the inclusive start date of the time window, if
                start_time == '' the start date is set as the first date in the relevant table
            end_time: (int, datetime) the non-inclusive end date of the time window, if
                end_time == '' the end date is set to the last date in the relevant table
            lb_type: (int) the type of LB to tally, options are:
                    '': all LBs
                    'm': missed LBs (scheduled labs which are not done)
                    'u': uncheduled LBs

        Returns:
            a pandas DataFrame with columns:
                Site: the site number
                KRI_Type: the KRI information
                Start_Time: the start_time for the KRI
                End_Time: the end_time for the KRI
                Value: the value of the KRI
                Units: the units of the KRI
        """
        active_subjects = self.active_subject_count(start_time, end_time)

        step_lb = self.lb_count(start_time, end_time, lb_type)
        step_lb = step_lb.merge(active_subjects, on="Site")
        step_lb.Value = step_lb.Value / step_lb.Subjects
        step_lb = step_lb.replace({np.inf: np.nan})
        step_lb["KRI_Type"] = lb_type.upper() + "LB_per_active_subject"
        step_lb = step_lb[step_lb.Value.notna()]
        step_lb = step_lb.drop("Subjects", axis=1)

        return step_lb

    def ip_variance(self, start_time, end_time):
        """
        Computes the standard deviation of patient dosage for each patient, group by site and average for a site-level score.

        Args:
            start_time: (int, datetime) the inclusive start date of the time window, if
                start_time == '' the start date is set as the first date in the relevant table
            end_time: (int, datetime) the non-inclusive end date of the time window, if
                end_time == '' the end date is set to the last date in the relevant table

        Returns:
            a pandas DataFrame with columns:
                Site: the site number
                KRI_Type: the KRI information
                Start_Time: the start_time for the KRI
                End_Time: the end_time for the KRI
                Value: the value of the KRI
                Units: the units of the KRI
        """
        if isinstance(start_time, int) and isinstance(end_time, int):
            in_range = self.EX[
                (start_time <= self.EX.EXSTDY) & (self.EX.EXSTDY < end_time)
            ]
        elif isinstance(start_time, pd.Timestamp) and isinstance(
            end_time, pd.Timestamp
        ):
            in_range = self.EX[
                (start_time <= self.EX.EXSTDTC) & (self.EX.EXSTDTC < end_time)
            ]
        else:
            raise NotImplementedError

        patient_var_group = in_range.groupby(["Site", "USUBJID"])
        patient_var = patient_var_group.EXDOSE.agg("std").fillna(0)
        site_var = patient_var.groupby("Site").agg("mean")
        site_var = site_var.reindex(self._site_list, fill_value=0)

        out = series_to_frame(
            site_var, "IP_variance", start_time, end_time, units="mg/sub"
        )

        return out

    def missed_dose(self, start_time, end_time):
        """
        Tally the total number missed doses of study drug in the specified time window.

        Args:
            start_time: (int, datetime) the inclusive start date of the time window, if
                start_time == '' the start date is set as the first date in the relevant table
            end_time: (int, datetime) the non-inclusive end date of the time window, if
                end_time == '' the end date is set to the last date in the relevant table

        Returns:
            a pandas DataFrame with columns:
                Site: the site number
                KRI_Type: the KRI information
                Start_Time: the start_time for the KRI
                End_Time: the end_time for the KRI
                Value: the value of the KRI
                Units: the units of the KRI
        """
        if isinstance(start_time, int) and isinstance(end_time, int):
            in_range = self.EX[
                (start_time <= self.EX.EXSTDY) & (self.EX.EXSTDY < end_time)
            ]
        elif isinstance(start_time, pd.Timestamp) and isinstance(
            end_time, pd.Timestamp
        ):
            in_range = self.EX[
                (start_time <= self.EX.EXSTDTC) & (self.EX.EXSTDTC < end_time)
            ]
        else:
            raise NotImplementedError

        try:
            in_type = in_range[
                in_range.EXADJ.apply(lambda x: str(x).split(":")[0]) == "Dose Withheld"
            ]
        except AttributeError:
            in_type = in_range[in_range.EXDOSE == 0]

        site_count = in_type.groupby("Site").USUBJID.agg("count")
        site_count = site_count.reindex(self._site_list, fill_value=0)

        out = series_to_frame(
            site_count, "Missed_Dose", start_time, end_time, units="dose/sub"
        )
        return out

    def numerical_trend_aggregation(
        self,
        domain,
        name_column,
        value_column,
        unit_column,
        time_column,
        start_time,
        end_time,
        time_step,
        category_column="",
    ):
        """
        Compute the average and st. dev. of data in time.

        Args:
            domain: (str) the name of the domain table to score
            name_column: (str) the column of the domain table which contains name info
            value_column: (str) the column of the domain table that contains the numerical
                values to score
            unit_column: (str) the column of the domain table that contains the units
                of the values
            time_column: (str) the column of the domain table that contains the time
                information
            start_time: (int, datetime) the inclusive start date of the time window, if
                start_time == '' the start date is set as the first date in the relevant table
            end_time: (int, datetime) the non-inclusive end date of the time window, if
                end_time == '' the end date is set to the last date in the relevant table
            category_column: (str) the category column of the domain table

        Returns:
            a pandas DataFrame with columns:
                Site: the site number
                KRI_Type: the KRI information
                Start_Time: the start_time for the KRI
                End_Time: the end_time for the KRI
                Value: the value of the KRI
                Units: the units of the KRI
        """
        category_flag = True
        if not category_column:
            category_column = name_column
            category_flag = False

        df = getattr(self, domain)
        df_numerical = df[df[unit_column].notna()]

        df_clean, df_drop = clean_units(
            df_numerical,
            name_column,
            value_column,
            unit_column,
            category_column=category_column,
            thresh=0.9,
        )

        data_counts = df_clean.groupby([category_column, name_column])[
            unit_column
        ].value_counts()
        total_subjects = self.active_subject_count(0, 1000, level="study")
        to_track_bool = data_counts > 3 * total_subjects
        to_track = data_counts[to_track_bool].index

        trend_df_list = []

        for category, name, units in to_track:
            category_bool = df_clean[category_column] == category
            name_bool = df_clean[name_column] == name
            in_type = df_clean[category_bool & name_bool]

            clean_name = name.lower().replace(" ", "_").replace(".", "")

            if (
                isinstance(start_time, int)
                and isinstance(end_time, int)
                and isinstance(time_step, int)
            ):

                for day in range(start_time, end_time, time_step):
                    stepped_day = day + time_step
                    left_bool = day <= in_type[time_column]
                    right_bool = in_type[time_column] < stepped_day
                    in_range = in_type[left_bool & right_bool]

                    in_range_mean = in_range.groupby("Site")[value_column].mean()
                    in_range_mean_df = series_to_frame(
                        in_range_mean,
                        clean_name + "_mean",
                        day,
                        stepped_day,
                        units=units,
                        category=category_flag * category,
                    )

                    trend_df_list.append(in_range_mean_df)

                    in_range_stdev = in_range.groupby("Site")[value_column].agg("std")
                    in_range_stdev_df = series_to_frame(
                        in_range_stdev,
                        clean_name + "_stdev",
                        day,
                        stepped_day,
                        units=units,
                        category=category_flag * category,
                    )

                    trend_df_list.append(in_range_stdev_df)

            elif (
                isinstance(start_time, pd.Timestamp)
                and isinstance(end_time, pd.Timestamp)
                and isinstance(time_step, pd.Timedelta)
            ):

                for date in pd.date_range(start_time, end_time, freq=time_step):
                    stepped_date = date + time_step
                    left_bool = date <= in_type[time_column]
                    right_bool = in_type[time_column] < stepped_date
                    in_range = in_type[left_bool & right_bool]

                    in_range_mean = in_range.groupby("Site")[value_column].mean()
                    in_range_mean_df = series_to_frame(
                        in_range_mean,
                        clean_name + "_mean",
                        date,
                        stepped_date,
                        units=units,
                        category=category_flag * category,
                    )

                    trend_df_list.append(in_range_mean_df)

                    in_range_stdev = in_range.groupby("Site")[value_column].agg("std")
                    in_range_stdev_df = series_to_frame(
                        in_range_stdev,
                        clean_name + "_stdev",
                        date,
                        stepped_date,
                        units=units,
                        category=category_flag * category,
                    )

                    trend_df_list.append(in_range_stdev_df)

        out = pd.concat(trend_df_list).reset_index(drop=True)
        return out

    def categorical_trend_aggregation(
        self, domain, name_column, time_column, start_time, end_time, time_step
    ):
        """
        Bin data between start_date and end_date into time periods of length step and compute the

        Args:
            domain: (str) the name of the domain table to score
            name_column: (str) the column of the domain table which contains name info
            value_column: (str) the column of the domain table that contains the numerical
                values to score
            unit_column: (str) the column of the domain table that contains the units
                of the values
            time_column: (str) the column of the domain table that contains the time
                information
            start_time: (int, datetime) the inclusive start date of the time window, if
                start_time == '' the start date is set as the first date in the relevant table
            end_time: (int, datetime) the non-inclusive end date of the time window, if
                end_time == '' the end date is set to the last date in the relevant table
            category_column: (str) the category column of the domain table

        Returns:
            a pandas DataFrame with columns:
                Site: the site number
                KRI_Type: the KRI information
                Start_Time: the start_time for the KRI
                End_Time: the end_time for the KRI
                Value: the value of the KRI
                Units: the units of the KRI
        """

        df = getattr(self, domain)

        trend_df_list = []

        if (
            isinstance(start_time, int)
            and isinstance(end_time, int)
            and isinstance(time_step, int)
        ):
            for day in range(start_time, end_time, time_step):
                stepped_day = day + time_step
                right_bool = df[time_column] < stepped_day
                cumulative = df[right_bool]
                left_bool = day <= cumulative[time_column]
                in_range = cumulative[left_bool]

                cumulative_counts = in_range[name_column].value_counts()
                total_counts = cumulative_counts.sum()
                cumulative_counts = cumulative_counts.rename("Study_counts")
                cumulative_counts = cumulative_counts.to_frame()
                cumulative_counts["Study_frequency"] = (
                    cumulative_counts["Study_counts"] / total_counts
                )

                site_counts = in_range.groupby("Site")[name_column].value_counts()

                to_add = {}

                for site in self._site_list:
                    if site in site_counts:
                        in_type = site_counts.loc[site].rename("Site_counts")
                        site_total_counts = in_type.sum()

                        comp_table = cumulative_counts.join(in_type)
                        comp_table = comp_table.fillna(0)
                        comp_table["Site_frequency"] = (
                            comp_table["Site_counts"] / site_total_counts
                        )
                        diff = (
                            comp_table["Study_frequency"] - comp_table["Site_frequency"]
                        )
                        to_add[site] = diff.abs().sum() / 2

                to_add = pd.Series(to_add).rename_axis("Site")

                to_add = series_to_frame(to_add, name_column + "_var", day, stepped_day)

                trend_df_list.append(to_add)

        elif (
            isinstance(start_time, pd.Timestamp)
            and isinstance(end_time, pd.Timestamp)
            and isinstance(time_step, pd.Timedelta)
        ):
            for date in pd.date_range(start_time, end_time, freq=time_step):
                stepped_date = date + time_step
                right_bool = df[time_column] < stepped_date
                cumulative = df[right_bool]
                left_bool = date <= cumulative[time_column]
                in_range = cumulative[left_bool]

                cumulative_counts = cumulative[name_column].value_counts()
                total_counts = cumulative_counts.sum()
                cumulative_counts = cumulative_counts.rename("Study_counts")
                cumulative_counts = cumulative_counts.to_frame()
                cumulative_counts["Study_frequency"] = (
                    cumulative_counts["Study_counts"] / total_counts
                )

                site_counts = in_range.groupby("Site")[name_column].value_counts()

                to_add = {}

                for site in self._site_list:
                    if site in site_counts:
                        in_type = site_counts.loc[site].rename("Site_counts")
                        site_total_counts = in_type.sum()

                        comp_table = cumulative_counts.join(in_type)
                        comp_table = comp_table.fillna(0)
                        comp_table["Site_frequency"] = (
                            comp_table["Site_counts"] / site_total_counts
                        )
                        diff = (
                            comp_table["Study_frequency"] - comp_table["Site_frequency"]
                        )
                        to_add[site] = diff.abs().sum() / 2

                to_add = pd.Series(to_add).rename_axis("Site")

                to_add = series_to_frame(
                    to_add, name_column + "_var", date, stepped_date
                )

                trend_df_list.append(to_add)

        out = pd.concat(trend_df_list).reset_index(drop=True)
        return out

    def KRI_plot(self, domain, KRI_Type, title=""):
        """
        Generates a plot of KRI scores over time.

        Args:
            domain: (str) the domain table to plot
            KRI_Type: (str) the KRI to plot
            title: (str) the title of the plot

        Returns:
            None
        """
        sfl_defaults()

        site_list = self._site_list
        site_list = sorted(site_list)
        num_sites = len(site_list)

        site_to_color = {
            site: idx / (num_sites - 1) for idx, site in enumerate(site_list)
        }
        cmap = cm.get_cmap("jet")
        fig, ax = plt.subplots()

        df = getattr(self, domain)
        df_in_type = df[df.KRI_Type == KRI_Type]

        if KRI_Type.split("_")[-1] == "stdev":
            mean_label = "Study St. Dev."
        else:
            mean_label = "Study Mean"

        df_in_type.groupby("Start_Time").Value.mean().plot(
            ax=ax, c="k", label=mean_label, linewidth=3.0
        )
        plt.legend()

        for site, group in df_in_type.groupby("Site"):
            group.plot(
                "Start_Time",
                "Value",
                ax=ax,
                legend=False,
                label="_",
                alpha=0.7,
                c=cmap(site_to_color[site]),
            )
        df_in_type.groupby("Start_Time").Value.mean().plot(
            ax=ax, c="k", label=mean_label, linewidth=3.0
        )
        #         plt.legend()

        cbar = plt.colorbar(cm.ScalarMappable(cmap=cmap), ax=ax)
        cbar.set_ticks(
            [guy / num_sites for guy in range(0, num_sites, int(num_sites / 8))]
        )
        cbar.set_ticklabels([guy for guy in range(0, num_sites, int(num_sites / 8))])
        cbar.set_label("Site Number")

        if isinstance(df_in_type.Start_Time.iloc[0], np.int64):
            plt.xlabel("Study Day")
        else:
            plt.xlabel("Date")

        lab = " ".join(KRI_Type.split("_")).title()
        y_lab = lab + f" ({df_in_type.Units.iloc[0]})"
        plt.ylabel(y_lab)

        y_lower = 0.9 * (df_in_type.Value.mean() - 3 * df_in_type.Value.std())

        y_upper = 1.1 * (df_in_type.Value.mean() + 5 * df_in_type.Value.std())

        y_lower = max(y_lower, 0)

        plt.ylim([y_lower, y_upper])
        plt.xlim(df_in_type.Start_Time.min(), df_in_type.Start_Time.max())

        if title:
            plt.title(title)
        else:
            plt.title(lab + " Trends")
        plt.show()

    def score_safety(self, start_time, end_time, time_step):
        """
        Score the Safety KRIS over time.

        Args:
            start_time: (int, datetime) the inclusive start date of the time window, if
                start_time is an int Study Day is used, if start_time is a Date the calendar
                date is used
            end_time: (int, datetime) the non-inclusive end date of the time window, if
                end_time is an int Study Day is used, if start_time is a Date the calendar
                date is used
            time_step: (int, timedelta) the number of days in each reporting period, if
                time_step is an int Study Day is used, if time_step is a timedelts the calendar
                date is used
        Returns:
            a pandas DataFrame with columns:
                Site: the site number
                KRI_Type: the KRI information
                Start_Time: the start_time for the KRI
                End_Time: the end_time for the KRI
                Value: the value of the KRI
                Units: the units of the KRI
        """
        safety_dfs = []

        if (
            isinstance(start_time, int)
            and isinstance(end_time, int)
            and isinstance(time_step, int)
        ):

            for day in range(start_time, end_time, time_step):
                stepped_day = day + time_step

                safety_dfs.extend(
                    [
                        self.ae_rate(day, stepped_day),
                        self.ae_rate(day, stepped_day, ae_type="s"),
                        self.ae_rate(day, stepped_day, ae_type="r"),
                        self.ae_rate(day, stepped_day, ae_type="sr"),
                    ]
                )
        elif (
            isinstance(start_time, pd.Timestamp)
            and isinstance(end_time, pd.Timestamp)
            and isinstance(time_step, pd.Timedelta)
        ):
            for date in pd.date_range(start_time, end_time, freq=time_step):
                stepped_date = date + time_step

                safety_dfs.extend(
                    [
                        self.ae_rate(date, stepped_date),
                        self.ae_rate(date, stepped_date, ae_type="s"),
                        self.ae_rate(date, stepped_date, ae_type="r"),
                        self.ae_rate(date, stepped_date, ae_type="sr"),
                    ]
                )
        else:
            raise NotImplementedError

        to_add = pd.concat(safety_dfs)
        self.Safety = self.Safety.append(to_add).reset_index(drop=True)

    def score_data_quality(self, start_time, end_time, time_step):
        """
        Score the Data Quality KRIS over time.

        Args:
            start_time: (int, datetime) the inclusive start date of the time window, if
                start_time is an int Study Day is used, if start_time is a Date the calendar
                date is used
            end_time: (int, datetime) the non-inclusive end date of the time window, if
                end_time is an int Study Day is used, if start_time is a Date the calendar
                date is used
            time_step: (int, timedelta) the number of days in each reporting period, if
                time_step is an int Study Day is used, if time_step is a timedelts the calendar
                date is used
        Returns:
            a pandas DataFrame with columns:
                Site: the site number
                KRI_Type: the KRI information
                Start_Time: the start_time for the KRI
                End_Time: the end_time for the KRI
                Value: the value of the KRI
                Units: the units of the KRI
        """

        dq_dfs = []
        if (
            isinstance(start_time, pd.Timestamp)
            and isinstance(end_time, pd.Timestamp)
            and isinstance(time_step, pd.Timedelta)
        ):
            for date in pd.date_range(start_time, end_time, freq=time_step):
                stepped_date = date + time_step

                dq_dfs.extend(
                    [
                        self.qy_rate(date, stepped_date),
                        self.qy_rate(date, stepped_date, qy_type="a"),
                        self.qy_rate(date, stepped_date, qy_type="m"),
                        self.qy_rate(date, stepped_date, qy_type="h"),
                        self.qy_response_time(date, stepped_date),
                    ]
                )
        else:
            raise NotImplementedError

        to_add = pd.concat(dq_dfs)
        self.Data_Quality = self.Data_Quality.append(to_add).reset_index(drop=True)

    def score_lb_management(self, start_time, end_time, time_step):
        """
        Score the Lab Management KRIS over time.

        Args:
            start_time: (int, datetime) the inclusive start date of the time window, if
                start_time is an int Study Day is used, if start_time is a Date the calendar
                date is used
            end_time: (int, datetime) the non-inclusive end date of the time window, if
                end_time is an int Study Day is used, if start_time is a Date the calendar
                date is used
            time_step: (int, timedelta) the number of days in each reporting period, if
                time_step is an int Study Day is used, if time_step is a timedelts the calendar
                date is used
        Returns:
            a pandas DataFrame with columns:
                Site: the site number
                KRI_Type: the KRI information
                Start_Time: the start_time for the KRI
                End_Time: the end_time for the KRI
                Value: the value of the KRI
                Units: the units of the KRI
        """

        lb_man_dfs = []

        if (
            isinstance(start_time, int)
            and isinstance(end_time, int)
            and isinstance(time_step, int)
        ):

            for day in range(start_time, end_time, time_step):
                stepped_day = day + time_step

                lb_man_dfs.extend(
                    [
                        self.lb_rate(day, stepped_day),
                        self.lb_rate(day, stepped_day, lb_type="u"),
                        self.lb_rate(day, stepped_day, lb_type="m"),
                    ]
                )
        elif (
            isinstance(start_time, pd.Timestamp)
            and isinstance(end_time, pd.Timestamp)
            and isinstance(time_step, pd.Timedelta)
        ):
            for date in pd.date_range(start_time, end_time, freq=time_step):
                stepped_date = date + time_step

                lb_man_dfs.extend(
                    [
                        self.lb_rate(date, stepped_date),
                        self.lb_rate(date, stepped_date, lb_type="u"),
                        self.lb_rate(date, stepped_date, lb_type="m"),
                    ]
                )
        else:
            raise NotImplementedError

        to_add = pd.concat(lb_man_dfs)
        self.LB_Management = self.LB_Management.append(to_add).reset_index(drop=True)

    def score_ip_management(self, start_time, end_time, time_step):
        """
        Score the IP Management KRIS over time.

        Args:
            start_time: (int, datetime) the inclusive start date of the time window, if
                start_time is an int Study Day is used, if start_time is a Date the calendar
                date is used
            end_time: (int, datetime) the non-inclusive end date of the time window, if
                end_time is an int Study Day is used, if start_time is a Date the calendar
                date is used
            time_step: (int, timedelta) the number of days in each reporting period, if
                time_step is an int Study Day is used, if time_step is a timedelts the calendar
                date is used
        Returns:
            a pandas DataFrame with columns:
                Site: the site number
                KRI_Type: the KRI information
                Start_Time: the start_time for the KRI
                End_Time: the end_time for the KRI
                Value: the value of the KRI
                Units: the units of the KRI
        """

        ip_man_dfs = []

        if (
            isinstance(start_time, int)
            and isinstance(end_time, int)
            and isinstance(time_step, int)
        ):

            for day in range(start_time, end_time, time_step):
                stepped_day = day + time_step

                ip_man_dfs.extend(
                    [
                        self.ip_variance(day, stepped_day),
                        self.missed_dose(day, stepped_day),
                    ]
                )
        elif (
            isinstance(start_time, pd.Timestamp)
            and isinstance(end_time, pd.Timestamp)
            and isinstance(time_step, pd.Timedelta)
        ):
            for date in pd.date_range(start_time, end_time, freq=time_step):
                stepped_date = date + time_step

                ip_man_dfs.extend(
                    [
                        self.ip_variance(date, stepped_date),
                        self.missed_dose(date, stepped_date),
                    ]
                )
        else:
            raise NotImplementedError

        to_add = pd.concat(ip_man_dfs)
        self.IP_Management = self.IP_Management.append(to_add).reset_index(drop=True)

    def score_subject_management(self, start_time, end_time, time_step):
        """
        Score the Subject Management KRIS over time.

        Args:
            start_time: (int, datetime) the inclusive start date of the time window, if
                start_time is an int Study Day is used, if start_time is a Date the calendar
                date is used
            end_time: (int, datetime) the non-inclusive end date of the time window, if
                end_time is an int Study Day is used, if start_time is a Date the calendar
                date is used
            time_step: (int, timedelta) the number of days in each reporting period, if
                time_step is an int Study Day is used, if time_step is a timedelts the calendar
                date is used
        Returns:
            a pandas DataFrame with columns:
                Site: the site number
                KRI_Type: the KRI information
                Start_Time: the start_time for the KRI
                End_Time: the end_time for the KRI
                Value: the value of the KRI
                Units: the units of the KRI
        """

        sub_man_dfs = []

        if (
            isinstance(start_time, int)
            and isinstance(end_time, int)
            and isinstance(time_step, int)
        ):

            for day in range(start_time, end_time, time_step):
                stepped_day = day + time_step
                subject_count = self.active_subject_count(day, stepped_day)
                subject_count = series_to_frame(
                    subject_count,
                    "Active_Subject_Count",
                    day,
                    stepped_day,
                    units="Sub",
                )

                sub_man_dfs.extend([subject_count])
        elif (
            isinstance(start_time, pd.Timestamp)
            and isinstance(end_time, pd.Timestamp)
            and isinstance(time_step, pd.Timedelta)
        ):
            for date in pd.date_range(start_time, end_time, freq=time_step):
                stepped_date = date + time_step

                sub_man_dfs.extend([self.active_subject_count(date, stepped_date)])
        else:
            raise NotImplementedError

        to_add = pd.concat(sub_man_dfs)
        self.SB_Management = self.SB_Management.append(to_add)
        
    def score_vital_trends(self, start_time, end_time, time_step):
        """
        Score the Vital Trends KRIS over time.

        Args:
            start_time: (int, datetime) the inclusive start date of the time window, if
                start_time is an int Study Day is used, if start_time is a Date the calendar
                date is used
            end_time: (int, datetime) the non-inclusive end date of the time window, if
                end_time is an int Study Day is used, if start_time is a Date the calendar
                date is used
            time_step: (int, timedelta) the number of days in each reporting period, if
                time_step is an int Study Day is used, if time_step is a timedelts the calendar
                date is used
        Returns:
            a pandas DataFrame with columns:
                Site: the site number
                KRI_Type: the KRI information
                Start_Time: the start_time for the KRI
                End_Time: the end_time for the KRI
                Value: the value of the KRI
                Units: the units of the KRI
        """

        if (
            isinstance(start_time, int)
            and isinstance(end_time, int)
            and isinstance(time_step, int)
        ):
            self.VS_Trend = self.numerical_trend_aggregation(
                "VS",
                "VSTEST",
                "VSSTRESN",
                "VSSTRESU",
                "VSDY",
                start_time,
                end_time,
                time_step,
            )
        elif (
            isinstance(start_time, pd.Timestamp)
            and isinstance(end_time, pd.Timestamp)
            and isinstance(time_step, pd.Timedelta)
        ):
            self.VS_Trend = self.numerical_trend_aggregation(
                "VS",
                "VSTEST",
                "VSSTRESN",
                "VSSTRESU",
                "VSDTC",
                start_time,
                end_time,
                time_step,
            )

    def score_lab_trends(self, start_time, end_time, time_step):
        """
        Score the Lab Trends KRIS over time.

        Args:
            start_time: (int, datetime) the inclusive start date of the time window, if
                start_time is an int Study Day is used, if start_time is a Date the calendar
                date is used
            end_time: (int, datetime) the non-inclusive end date of the time window, if
                end_time is an int Study Day is used, if start_time is a Date the calendar
                date is used
            time_step: (int, timedelta) the number of days in each reporting period, if
                time_step is an int Study Day is used, if time_step is a timedelts the calendar
                date is used
        Returns:
            a pandas DataFrame with columns:
                Site: the site number
                KRI_Type: the KRI information
                Start_Time: the start_time for the KRI
                End_Time: the end_time for the KRI
                Value: the value of the KRI
                Units: the units of the KRI
        """

        if (
            isinstance(start_time, int)
            and isinstance(end_time, int)
            and isinstance(time_step, int)
        ):
            self.LB_Trend = self.numerical_trend_aggregation(
                "LB",
                "LBTEST",
                "LBSTRESN",
                "LBSTRESU",
                "LBDY",
                start_time,
                end_time,
                time_step,
                category_column="LBCAT",
            )
        elif (
            isinstance(start_time, pd.Timestamp)
            and isinstance(end_time, pd.Timestamp)
            and isinstance(time_step, pd.Timedelta)
        ):
            self.VS_Trend = self.numerical_trend_aggregation(
                "LB",
                "LBTEST",
                "LBSTRESN",
                "LBSTRESU",
                "LBDTC",
                start_time,
                end_time,
                time_step,
            )

    def score_all(self, start_time, end_time, time_step):
        """
        Score all the KRIS over time.

        Args:
            start_time: (int, datetime) the inclusive start date of the time window, if
                start_time is an int Study Day is used, if start_time is a Date the calendar
                date is used
            end_time: (int, datetime) the non-inclusive end date of the time window, if
                end_time is an int Study Day is used, if start_time is a Date the calendar
                date is used
            time_step: (int, timedelta) the number of days in each reporting period, if
                time_step is an int Study Day is used, if time_step is a timedelts the calendar
                date is used
        Returns:
            None
        """

        if (
            isinstance(start_time, int)
            and isinstance(end_time, int)
            and isinstance(time_step, int)
        ):
            self.score_safety(start_time, end_time, time_step)
            self.score_lb_management(start_time, end_time, time_step)
            self.score_ip_management(start_time, end_time, time_step)
            self.score_subject_management(start_time, end_time, time_step)
            self.score_vital_trends(start_time, end_time, time_step)
            self.score_lab_trends(start_time, end_time, time_step)

        elif (
            isinstance(start_time, pd.Timestamp)
            and isinstance(end_time, pd.Timestamp)
            and isinstance(time_step, pd.Timedelta)
        ):
            self.score_safety(start_time, end_time, time_step)
            self.score_data_quality(start_time, end_time, time_step)
            self.score_lb_management(start_time, end_time, time_step)
            self.score_ip_management(start_time, end_time, time_step)
#             self.score_subject_management(start_time, end_time, time_step)
            self.score_vital_trends(start_time, end_time, time_step)
            self.score_lab_trends(start_time, end_time, time_step)
