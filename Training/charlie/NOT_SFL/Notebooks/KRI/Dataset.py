import pandas as pd


class Dataset:
    def __init__(self, dataset=pd.DataFrame(), path="", params={}):
        self.path = path
        self.dataset = dataset
        self.raw = True

        for key, value in params.items():
            setattr(self, key, value)

        if path:
            self.load_from_path()

    def load_from_path(self):
        try:
            self.dataset = pd.read_csv(self.path)
        except FileNotFoundError:
            print("File not found")

    def get_time_column_name(self, time_range):
        return getattr(self, time_range.start_column)

    def get_site_array(self):
        if self.raw:
            print("Data must be preprocessed before site extraction")
            raise ValueError
        else:
            return self.dataset["Site"].unique()

    def get_data_from_time_range(self, time_range):
        time_column_name = self.get_time_column_name(time_range)

        left_bool = time_range.start_time <= self.dataset[time_column_name]
        right_bool = self.dataset[time_column_name] < time_range.end_time

        return self.dataset[left_bool & right_bool]
