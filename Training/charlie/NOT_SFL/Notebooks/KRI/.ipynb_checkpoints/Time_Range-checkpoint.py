import pandas as pd
import numpy as np


class Time_Range:
    def __init__(self, start_time, end_time, time_step=0):
        if isinstance(start_time, str):
            start_time = pd.Timestamp(start_time)
        if isinstance(end_time, str):
            end_time = pd.Timestamp(end_time)
        if isinstance(time_step, str):
            time_step = pd.Timedelta(time_step)
        if isinstance(start_time, np.int64):
            start_time = int(start_time)
        if isinstance(end_time, np.int64):
            end_time = int(end_time)

        self.start_time = start_time
        self.end_time = end_time
        self.time_step = time_step

        self.type_check()

    def type_check(self):
        if not self.time_step:
            if isinstance(self.start_time, int) and isinstance(self.end_time, int):
                self.type = "int"
                self.iterable = False
                self.start_column = "start_day"
                self.end_column = "end_day"
            elif isinstance(self.start_time, pd.Timestamp) and isinstance(
                self.end_time, pd.Timestamp
            ):
                self.type = "datetime"
                self.iterable = False
                self.start_column = "start_date"
                self.end_column = "end_date"
            else:
                print("Start Time and End Time must both be int or datetime")
                raise ValueError
        else:
            if (
                isinstance(self.start_time, int)
                and isinstance(self.end_time, int)
                and isinstance(self.time_step, int)
            ):
                self.type = "int"
                self.iterable = True
                self.start_column = "start_day"
                self.end_column = "end_day"
            elif (
                isinstance(self.start_time, pd.Timestamp)
                and isinstance(self.end_time, pd.Timestamp)
                and isinstance(self.time_step, pd.Timedelta)
            ):
                self.type = "datetime"
                self.iterable = True
                self.start_column = "start_date"
                self.end_column = "end_date"
            else:
                print("Start Time, End Time, and Timestep must all be int or datetime")
                raise ValueError

    def __iter__(self):
        if self.iterable:
            if self.type == "int":
                yield from range(self.start_time, self.end_time, self.time_step)
            else:
                yield from pd.date_range(
                    self.start_time, self.end_time, freq=self.time_step
                )
        else:
            print("Not iterable")
            raise ValueError
